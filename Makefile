
build:
	docker build -f ops/Dockerfile -t registry.gitlab.com/sebohe/world:latest .

service:
	docker service create --name gatsby -p 8000:8000 registry.gitlab.com/sebohe/world:latest

remove:
	docker service rm gatsby
