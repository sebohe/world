import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../../../../components/layout';
import Gallery from '../../../../components/Gallery';

import Images from './Images'

class HomeIndex extends React.Component {

  constructor() {
    super();
    this.state = {
      lightboxIsOpen: false,
      currentImage: 0,
    };
  }

  render() {
    const siteTitle = 'Week One';
    const siteDescription = 'San Francisco to Prague';

    return (
      <Layout>
        <Helmet>
          <title>{siteTitle}</title>
          <meta
            name='description' content={siteDescription}
          />
        </Helmet>

        <div id='main'>

          <section id='one'>
            <header className='major'>
              <h2>My last weeks in the PNW... and a new lifestyle</h2>
            </header>
          </section>

          <section id='text'>
            <p>
      It has been some crazy couple of months, It all started with the first leg of the trip in San Francisco. I attended an Ethereum hackathon and managed to build a dai ethereless wallet along with my partner. Lots of good things happened at that event, made a good friend that is trying to solve the monetary crisis in Venezuela. The first four pictures are from SF. Our team didn't win anything but I met some people that worked on exactly the same project, which is awesome because now we can rely on their project.
            </p>
            <p>
      I then had to move out of my apartment and leave some awesome roommates. They accepted me into their home about three years ago and we all got a long really well. It was a hard decision to make. The PNW is a beautiful part of the world, but I wanted to experience not having anywhere specific to go to and let the winds behind my back decide for me.
            </p>
            <p>
      On October 23rd 2018, I took the first couple of steps out of my apartment. I was then overwhelmed with a really strange feeling, starting a new adventure. Confident where I was going but at the same time scared. I assume that Frodo Baggins felt very similar to this. When I arrived at Prague I took the bus, the metro and tram to my new apartment. I LOOOOVE the public transportation in Prague. You won't get me to shut up about it if I ever see you again. Before the main conference, Devcon, I went to another smaller <a href="https://hackathon.status.im/">hackathon</a>. Here, I got to represent the company I work for <a href="https://althea.org">althea.org</a>. From the first day of the #crytpolife hackthon until Sunday, November 4th 2018 it feels like a blur. I pretty much just spent some great times with friends from the Ethereum ecosystem. Went to an announcement party of a new project in an extremely extravant venue with free steaks and free beer, so I got entertained by crypto millionaires which is nice. Imagine it like an Elon Musk launch party of some sort. I loved the always present background sound of electronic music.
            </p>

            <p>
      Anyways, as Anna would say and anyone who has been in Prague since late October. "I'm tired of talking about crypto!!. Let's talk about something else! Please I beg you! My ear drums bleed everytime I hear the word hash!". So Saturday, November 3rd and the following Sunday was a good day to explore Prague. We first went to Franz Kafka museum. A truly inmmersive museum, one could really get in touch with how Kafka felt. My interpretation is that he felt trapped in Prague, this created an internal preasure that allowed him write what he wanted to write. The museum revealed to me how stuck we can get in our <a href="https://www.youtube.com/watch?v=VaRO5-V1uK0">heads</a>... that the real prison is the prison of the mind.  Definitley going to pick up some of his works, I'm a sucker for tormented minds. But the museum is really worth it if you are in Prague. Another of the few stops include the astronomical clock, Charles Bridge and Prague Castle. Prague is a great city. It has a spirit and a lot to offer. Random events pop up everywhere and it has lots of hidden gems. Like that sweet art gallery plus cafe, <a href="https://www.google.com/maps/uv?hl=en&pb=!1s0x470b94eb3548b78f%3A0x11418fbcac316d13!2m17!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e6!2m2!1m1!1e4!2m2!1m1!1e5!3m1!7e115!4shttps%3A%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipNDfQhIkjEtLAlYM7rH38-0tVdsCjrvAWmwGDss%3Dw256-h144-k-no!5shlina%20cafe%20prague%20-%20Google%20Search&imagekey=!1e10!2sAF1QipNDfQhIkjEtLAlYM7rH38-0tVdsCjrvAWmwGDss&sa=X&ved=2ahUKEwiG3fe8mtHeAhUEJFAKHX5ZDNwQoiowGHoECAQQBg&activetab=main">hlina cafe</a>.
            </p>

            <p>
      So finally, after eveyone from Ethereum left Prague, I could finally go to the Institute of Cryptoanarchy (yes, I said that word again). Which is a sweet coffe shop that only takes Bitcoin as payment, and was told they will soon be taking some Dai (yaay). For the past three days I have been staying at <a href="https://www.sirtobys.com/">Sir Toby's Hostel</a> which has a great vibe and fun night activities. We played some trivia on Monday night and Beer pong last night (first time I ever played beer pong!). It is a fun hostel and good people. This hostel captures the appeal of European hostels. My next stop after this is going to be Budapest to meet another crypto nomad that is currently there. I leave on Friday, November 9th. Now I just get to work, travel, and sleep.
            </p>

          </section>

          <section id='gallery'>
            <Images />
          </section>
        </div>
      </Layout>
    );
  }
}

export default HomeIndex;
