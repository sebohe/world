import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../../../../components/layout';
import Gallery from '../../../../components/Gallery';

import Images from './Images'

class HomeIndex extends React.Component {

  constructor() {
    super();

    this.state = {
      lightboxIsOpen: false,
      currentImage: 0,
    };
  }

  render() {
    const siteTitle = 'Budapest';
    const siteDescription = 'Prague to Budapest';

    return (
      <Layout>
        <Helmet>
          <title>{siteTitle}</title>
          <meta
            name='description' content={siteDescription}
          />
        </Helmet>

        <div id='main'>

          <section id='one'>
            <header className='major'>
              <h2>Onwards to Budapest!</h2>
            </header>
          </section>

          <section id='text'>
            <p>
            </p>
            <p>
            </p>
            <p>
            </p>
            <p>
            </p>
            <p>
           Till next time! 
            </p>
          </section>

          <section id='gallery'>
            <Images />
          </section>
        </div>
      </Layout>
    );
  }
}

export default HomeIndex;
