import React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'gatsby';

import Layout from '../components/layout';

class HomeIndex extends React.Component {

  constructor() {
    super();

    this.state = {
      lightboxIsOpen: false,
      currentImage: 0,
    };
  }

  render() {
    const siteTitle = 'Seabass\' personal blog';
    const siteDescription = '';

    return (
      <Layout>
        <Helmet>
          <title>{siteTitle}</title>
          <meta
            name="description" content={siteDescription}
          />
        </Helmet>

        <div id="main">

          <section id="one">
            <header className="major">
              <h2>Welcome to my corner of the world</h2>
            </header>
          </section>

          <section id="two">
            <p>
      Hello everyone! I finally have a way of letting everyone know where I'm at, what I'm doing. Sorry for the delay but it has been some hectic two months. This blog is a little experiment of mine. I want to write my thoughts about <a href="http://www.paulgraham.com/essay.html">things</a> here and also just get better with web dev and writing. Writing things down is one of the best ways to improve speech, which is something I feel like I could improve on. Without further ado, here are some quick updates.
            </p>
          </section>

          <section id="three">
            <h2>Recent Posts</h2>
            <div>
              <Link to="/blog/europe/prague">
               Prague
              </Link>
            </div>
            <div>
              <Link to="/blog/europe/budapest">
               Budapest
              </Link>
            </div>
            <div>
              <Link to="/blog/europe/cracovia">
               Krakow
              </Link>
            </div>
          </section>

          <section id="four">
            <div className="row">
              <p>Last known location: Amsterdam, 20th of Decemver, 2018</p>
            </div>
          </section>

        </div>

      </Layout>
    );
  }
}

export default HomeIndex;
