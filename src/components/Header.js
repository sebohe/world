import React from 'react'

import Footer from './Footer'
import avatar from '../assets/images/avatar.jpg'

// Why? how? what?
class Header extends React.Component {
  render() {
    return (
      <header id="header">
        <div className="inner">
          <a href="/" className="image avatar"><img src={avatar} alt="" /></a>
          <h1>I'm a wannabe linux guru and <br />
          fulltime time web3 developer <br />
          that has recently become a digital <br />
          nomad.
          </h1>
        </div>
        <Footer />
      </header>
    )
  }
}

export default Header
